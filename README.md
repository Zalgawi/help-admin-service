# Help Admin Service

A simple API that contains GET, POST, PUT & DELETE Methods.

## Description
A Java Springboot API that contains all basic CRUD features. The API is run locally and temporarily stores the objects (cards) temporarily within local memory.


## Links | Examples
More examples can be seen on https://www.getpostman.com/collections/00b881cc92dcab81c360